// SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package org.getdisconnected.metaapp.api;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LabNetworkChecker extends AbstractThreadedApi<LabNetworkChecker.LabNetworkUpdateListener>{

    private final List<String> MetalabSSIDS = Collections.unmodifiableList(Arrays.asList("Metalab", "Metalab"));

    public void checkNetworkStatusNow() {
        apiHandler.post(new Runnable() {
            @Override
            public void run() {
                // TODO: actually implement this
                final boolean inMetalab = true;

                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (listeners) {
                            for (LabNetworkUpdateListener listener : listeners) {
                                listener.networkStatusUpdate(inMetalab);
                            }
                        }
                    }
                });
            }
        });
    }

    public interface LabNetworkUpdateListener {
        void networkStatusUpdate(boolean inMetalab);
    }
}
