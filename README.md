<!--
SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
SPDX-License-Identifier: GPL-3.0-or-later
-->

# MetaApp

Unofficial companion App for Metalab

<img alt="logo" src="https://codeberg.org/uniqx/MetaApp/media/branch/master/app/src/main/res/mipmap-hdpi/ic_launcher_round.png" />

Features:

• Event calendar: Displays informations about upcoming events at Metalab.

• Door status: Displays whether Metalab is currently open or closed.

• Lighting controls: Allows you to change the colors of the light installation in Metalab main room.

Metalab is a hackerspace in Vienna, Austria.

https://metalab.at

<a href="https://f-droid.org/packages/at.h4x.metaapp">
  <img alt="Get it on F-Droid" src="https://codeberg.org/uniqx/MetaApp/media/branch/master/dev-assets/fdroid-badge.png" height="80" />
</a>

## Screenshots

<a href="https://codeberg.org/uniqx/MetaApp/media/branch/master/metadata/en-US/images/phoneScreenshots/screenshot1.png">
  <img alt="Screenshot: Door status and calendar" src="https://codeberg.org/uniqx/MetaApp/media/branch/master/metadata/en-US/images/phoneScreenshots/screenshot1.png" width="150" />
</a>

<a href="https://codeberg.org/uniqx/MetaApp/media/branch/master/metadata/en-US/images/phoneScreenshots/screenshot2.png">
  <img alt="Screenshot: umbrella lighting controls" src="https://codeberg.org/uniqx/MetaApp/media/branch/master/metadata/en-US/images/phoneScreenshots/screenshot2.png" width="150" />
</a>

## License

GPL-3.0-or-later
