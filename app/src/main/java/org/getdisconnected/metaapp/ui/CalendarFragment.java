// SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package org.getdisconnected.metaapp.ui;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.getdisconnected.metaapp.R;
import org.getdisconnected.metaapp.api.ApiManger;
import org.getdisconnected.metaapp.api.CalendarApi;
import org.getdisconnected.metaapp.api.HodorsCyberCoffeeApi;

import java.util.List;

/**
 * A fragment representing a list of Items.
 */
public class CalendarFragment extends Fragment {

    private SwipeRefreshLayout swipeRefreshLayout;
    private View doorOpenOverlay;
    private TextView doorStatusMessage;

    public CalendarFragment() {
        // mandatory default constructor
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static CalendarFragment newInstance(int columnCount) {
        CalendarFragment fragment = new CalendarFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);

        // bind actionbar to activity, aka. make actionbar behave normal
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        doorOpenOverlay = view.findViewById(R.id.door_open_overlay);
        doorStatusMessage = view.findViewById(R.id.door_status_message);

        // Set the adapter
        RecyclerView recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setAdapter(new CalendarRecyclerViewAdapter(view.getContext()));

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        swipeRefreshLayout.setRefreshing(true);

        ApiManger.getCalendarApi(getContext()).addUpdateListener(calenderListener);
        ApiManger.getHodorsCyberCoffeApi().addUpdateListener(hodorsCyberCoffeListener);
        ApiManger.getHodorsCyberCoffeApi().checkDoorStatusNow();

        return view;
    }

    @Override
    public void onDestroyView() {
        ApiManger.getCalendarApi(getContext()).removeUpdateListener(calenderListener);
        ApiManger.getHodorsCyberCoffeApi().removeUpdateListener(hodorsCyberCoffeListener);
        super.onDestroyView();
    }

    private CalendarApi.CalendarListener calenderListener = new CalendarApi.CalendarListener() {
        @Override
        public void umbrellaUpdated(List<CalendarApi.CalendarItem> calendarItems) {
            swipeRefreshLayout.setRefreshing(false);
        }
    };

    private HodorsCyberCoffeeApi.DoorStatusListener hodorsCyberCoffeListener = new HodorsCyberCoffeeApi.DoorStatusListener() {
        @Override
        public void statusUpdate(HodorsCyberCoffeeApi.DoorStatus doorStatus) {
            switch (doorStatus) {
                case CLOSED:
                    doorOpenOverlay.animate().setDuration(200).alpha(0.0F).start();
                    doorStatusMessage.setText(R.string.door_closed_message);
                    return;
                case OFFLINE:
                    doorOpenOverlay.animate().setDuration(200).alpha(0.0F).start();
                    doorStatusMessage.setText(R.string.door_offline_message);
                    return;
                case OPEN:
                    doorOpenOverlay.animate().setDuration(200).alpha(1.0F).start();
                    doorStatusMessage.setText(R.string.door_open_message);
                    return;
            }
        }
    };

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            ApiManger.getHodorsCyberCoffeApi().checkDoorStatusNow();
            ApiManger.getCalendarApi(getContext()).requestCalenderUpdate();
        }
    };

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_action_about:
                AboutDialogBuilder.create(getContext());
                return true;
            default:
                return false;
        }
    }
}
