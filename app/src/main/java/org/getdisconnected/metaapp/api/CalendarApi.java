// SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package org.getdisconnected.metaapp.api;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import org.getdisconnected.metaapp.data.UmbrellaApiPrefs;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class CalendarApi extends AbstractThreadedApi<CalendarApi.CalendarListener>{

    private final Context context;

    private final int artnetUniverse = 3;
    private final int artnetSubnet = 0;
    private final String ip = "10.20.255.255"; // n.b. broadcast

    private final byte[] colorBuffer = new byte[UmbrellaApiPrefs.UMBRELLA_COUNT*5];

    CalendarApi(final Context context) {
        super();
        this.context = context;
    }

    public void requestCalenderUpdate(){
        apiHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    List<CalendarItem> calendarItems = new ArrayList<>();
                    URL url = new URL("https://metalab.at/calendar");
                    URLConnection c = url.openConnection();
                    c.connect();
                    Document doc = Jsoup.parse(c.getInputStream(), c.getContentEncoding(), "https://metalab.at/calendar");
                    for (Element calItem : doc.select(".event")) {
                        Log.d("###", calItem.html());
                        CalendarItem newItem = new CalendarItem();
                        newItem.setTitle(calItem.select("a").first().html());
                        newItem.setUrl(calItem.select("a").first().attr("href"));
                        if (!newItem.getUrl().startsWith("https://") || !newItem.getUrl().startsWith("http://")) {
                            newItem.setUrl("https://metalab.at/wiki" + newItem.getUrl());
                        }
                        if (calItem.select(".teaser") != null) {
                            newItem.setTeaser(calItem.select(".teaser").html());
                        }
                        if (calItem.select(".categoryName") != null) {
                            newItem.setCategoryName(calItem.select(".categoryName").html());
                            newItem.setCategoryLink(calItem.select(".categoryName").attr("href"));
                        }
                        if (calItem.select(".location > a") != null) {
                            newItem.setRoom(calItem.select(".location > a").html());
                        }
                        newItem.setId(calItem.select("a.hoverHidden").attr("href").split("/")[3]);
                        newItem.setIcalLink("https://metalab.at" + calItem.select("a").get(0).attr("href"));

                        String rawdate = calItem.select(".event_date").html();
                        if (rawdate.length() == 28) {
                            newItem.setDay(rawdate.substring(0, 3));
                            newItem.setDate(rawdate.substring(4, rawdate.length() - 14));
                            newItem.setEndTime(rawdate.substring(rawdate.length() - 5));
                            newItem.setStartTime(rawdate.substring(rawdate.length() - 13, rawdate.length() - 8));
                        } else if (rawdate.length() == 20) {
                            newItem.setDay(rawdate.substring(0, 3));
                            newItem.setDate(rawdate.substring(4, rawdate.length() - 6));
                            newItem.setStartTime(rawdate.substring(rawdate.length() - 5));
                            newItem.setEndTime("");
                        } else {
                            newItem.setDate("");
                            newItem.setDay("");
                            newItem.setStartTime("");
                            newItem.setEndTime("");
                            Log.e("###", rawdate.length() + " is a bad length for rawdate: " + rawdate);
                        }
                        calendarItems.add(newItem);
                    }
                    notifyCalendarUpdate(calendarItems);
                } catch (MalformedURLException e) {
                    notifyCalendarUpdate(new ArrayList<CalendarItem>());
                    Log.e("###", "could not check calendar", e);
                } catch (IOException e) {
                    notifyCalendarUpdate(new ArrayList<CalendarItem>());
                    Log.e("###", "could not check calendar", e);
                }
            }
        });
    }

    public interface CalendarListener {
        void umbrellaUpdated(List<CalendarItem> calendarItems);
    }

    private void notifyCalendarUpdate(final List<CalendarItem> calendarItems) {
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                synchronized (listeners) {
                    for (CalendarListener listener : listeners) {
                        listener.umbrellaUpdated(calendarItems);
                    }
                }
            }
        });
    }

    public static class CalendarItem {
        String id;
        String title;
        String teaser;
        String url;
        String room;
        String categoryName;
        String categoryLink;
        String date;
        String day;
        String startTime;
        String endTime;
        String icalLink;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getTitle() {
            return title;
        }

        public String getTeaser() {
            return teaser;
        }

        public void setTeaser(String teaser) {
            this.teaser = teaser;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getRoom() {
            return room;
        }

        public void setRoom(String room) {
            this.room = room;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getCategoryLink() {
            return categoryLink;
        }

        public void setCategoryLink(String categoryLink) {
            this.categoryLink = categoryLink;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getDay() {
            return day;
        }

        public void setDay(String day) {
            this.day = day;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getIcalLink() {
            return icalLink;
        }

        public void setIcalLink(String icalLink) {
            this.icalLink = icalLink;
        }

        @NonNull
        @Override
        public String toString() {
            return "cal item: " + id + ", " + date + ", " + startTime + ", " + endTime + ", " + title + ", " + teaser + ", " + categoryName + ", " + categoryLink;
        }
    }
}
