Features:

• Event calendar: Displays informations about upcoming events at Metalab.

• Door status: Displays whether Metalab is currently open or closed.

• Lighting controls: Allows you to change the colors of the light installation in Metalab main room.

Metalab is a hackerspace in Vienna, Austria.

This app fetches data from Metalabs website and Metalabs door status api. Sadly both are not licenses as free software.

https://metalab.at
