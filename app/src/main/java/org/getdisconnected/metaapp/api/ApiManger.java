// SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package org.getdisconnected.metaapp.api;

import android.content.Context;

/**
 * stupid self-built dependency injection thingy.
 */
public class ApiManger {

    private static HodorsCyberCoffeeApi hodorsCyberCoffeeApi = null;
    private static LabNetworkChecker labNetworkChecker = null;
    private static UmbrellaApi umbrellaApi = null;
    private static CalendarApi calendarApi;

    public static synchronized HodorsCyberCoffeeApi getHodorsCyberCoffeApi(){
        if (hodorsCyberCoffeeApi == null) {
            hodorsCyberCoffeeApi = new HodorsCyberCoffeeApi();
        }
        return hodorsCyberCoffeeApi;
    }

    public static synchronized LabNetworkChecker getLabNetworkChecker() {
        if (labNetworkChecker == null) {
            labNetworkChecker = new LabNetworkChecker();
        }
        return labNetworkChecker;
    }

    public static synchronized UmbrellaApi getUmbrellaApi(Context context) {
        if (umbrellaApi == null) {
            umbrellaApi = new UmbrellaApi(context.getApplicationContext());
        }
        return umbrellaApi;
    }

    public static synchronized CalendarApi getCalendarApi(Context context) {
        if (calendarApi == null) {
            calendarApi = new CalendarApi(context);
        }
        return calendarApi;
    }
}
