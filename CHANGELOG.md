# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.0.1] - 2020-04-23
### Fixed
- fixed de translation F-Droid store listing
- fixed en translation typos
- fix crashing controls ui on old android version

## [v1.0.0] - 2020-04-12
### Added
- inital release

[Unreleased]: https://codeberg.org/getdisconnected/LibreIpsum/src/branch/master
[v1.0.1]: https://codeberg.org/getdisconnected/LibreIpsum/src/tag/v1.0.1
[v1.0.0]: https://codeberg.org/getdisconnected/LibreIpsum/src/tag/v1.0.0
