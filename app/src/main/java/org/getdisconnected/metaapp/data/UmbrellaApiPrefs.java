// SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package org.getdisconnected.metaapp.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

public class UmbrellaApiPrefs {

    public static final int UMBRELLA_COUNT = 12;

    private final SharedPreferences prefs;

    public UmbrellaApiPrefs(Context context) {
        prefs = context.getSharedPreferences("umbrella.preferences", Context.MODE_PRIVATE);
    }

    public void storeColorBuffer(byte[] colorData){
        prefs.edit().putString("color_buffer", Base64.encodeToString(colorData, Base64.DEFAULT)).commit();
    }

    public byte[] loadColorBuffer() {
        return Base64.decode(prefs.getString("color_buffer", ""), Base64.DEFAULT);
    }
}
