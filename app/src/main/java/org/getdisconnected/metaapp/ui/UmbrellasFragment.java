// SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package org.getdisconnected.metaapp.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.getdisconnected.metaapp.R;
import org.getdisconnected.metaapp.api.ApiManger;
import org.getdisconnected.metaapp.api.UmbrellaApi;
import org.getdisconnected.metaapp.data.UmbrellaApiPrefs;

public class UmbrellasFragment extends Fragment {

    private UmbrellaApi umbrellaApi;
    private UmbrellaAdapter umbrellaAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        umbrellaApi = ApiManger.getUmbrellaApi(getContext());

        View root = inflater.inflate(R.layout.fragment_umbrellas, container, false);

        umbrellaAdapter = new UmbrellaAdapter(new UmbrellaApiPrefs(getContext()), umbrellaApi) ;
        RecyclerView umbrellaGrid = root.findViewById(R.id.umbrella_grid);
        umbrellaGrid.setLayoutManager(new GridLayoutManager(getContext(), 3));
        umbrellaGrid.setAdapter(umbrellaAdapter);

        final Button randEverything = root.findViewById(R.id.rand_everything);
        randEverything.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                umbrellaApi.braodcastFullRand();
            }
        });

        final Button whiteEverything = root.findViewById(R.id.white_everything);
        whiteEverything.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                umbrellaApi.braodcastWhite((byte)255, (byte)255);
            }
        });

        final Button blackEverything = root.findViewById(R.id.black_everything);
        blackEverything.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                umbrellaApi.braodcastWhite((byte)0, (byte)0);
            }
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        umbrellaAdapter.onResume();
    }

    @Override
    public void onPause() {
        umbrellaAdapter.onPause();
        super.onPause();
    }
}
