// SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package org.getdisconnected.metaapp.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.getdisconnected.metaapp.api.ApiManger;
import org.getdisconnected.metaapp.R;
import org.getdisconnected.metaapp.api.HodorsCyberCoffeeApi;


public class DoorStatusFragment extends Fragment {

    private TextView textView;
    private HodorsCyberCoffeeApi hodorsCyberCoffeApi;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_door_status, container, false);
        textView = root.findViewById(R.id.text_home);

        hodorsCyberCoffeApi = ApiManger.getHodorsCyberCoffeApi();
        hodorsCyberCoffeApi.addUpdateListener(doorStatusListener);
        hodorsCyberCoffeApi.checkDoorStatusNow();

        return root;
    }

    @Override
    public void onDestroyView() {
        hodorsCyberCoffeApi.removeUpdateListener(doorStatusListener);
        super.onDestroyView();
    }

    private final SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            hodorsCyberCoffeApi.checkDoorStatusNow();
        }
    };

    private final HodorsCyberCoffeeApi.DoorStatusListener doorStatusListener = new HodorsCyberCoffeeApi.DoorStatusListener() {
        @Override
        public void statusUpdate(HodorsCyberCoffeeApi.DoorStatus doorStatus) {
            switch (doorStatus) {
                case OPEN:
                    textView.setText(R.string.door_open_message);
                    break;
                case CLOSED:
                    textView.setText(R.string.door_closed_message);
                    break;
                case OFFLINE:
                    textView.setText(R.string.door_offline_message);
            }
        }
    };

}
