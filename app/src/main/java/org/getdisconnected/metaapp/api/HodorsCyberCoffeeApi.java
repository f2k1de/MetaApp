// SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package org.getdisconnected.metaapp.api;

import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class HodorsCyberCoffeeApi extends AbstractThreadedApi<HodorsCyberCoffeeApi.DoorStatusListener>{

    public void checkDoorStatusNow() {
        apiHandler.post(new Runnable() {
            @Override
            public void run() {
                URLConnection c;
                try {
                    URL url = new URL("https://hodors.cyber.coffee/status.json");
                    c = url.openConnection();
                    c.setReadTimeout(6*1000);
                    JsonReader r = new JsonReader(new InputStreamReader(c.getInputStream()));
                    r.beginObject();
                    while (r.hasNext()) {
                        String name = r.nextName();
                        String value = r.nextString();
                        if ("status".equals(name)) {
                            if ("open".equals(value)) {
                                notifyStatusUpdate(DoorStatus.OPEN);
                                return;
                            } else if ("closed".equals(value)) {
                                notifyStatusUpdate(DoorStatus.CLOSED);
                                return;
                            }
                        }
                    }
                } catch (MalformedURLException e) {
                    Log.e("###", "could not check door status", e);
                } catch (IOException e) {
                    Log.e("###", "could not check door status", e);
                }

                notifyStatusUpdate(DoorStatus.OFFLINE);
                return;
            }
        });
    }

    private void notifyStatusUpdate(final DoorStatus status) {
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                synchronized (listeners) {
                    for (DoorStatusListener doorStatusListener : listeners) {
                        doorStatusListener.statusUpdate(status);
                    }
                }
            }
        });
    }

    public interface DoorStatusListener {
        void statusUpdate(DoorStatus doorStatus);
    }

    public static enum DoorStatus {

        /**
         * The door is open
         */
        OPEN,

        /**
         * The door is closed
         */
        CLOSED,

        /**
         * could not connect either the app or the service is offline
         */
        OFFLINE;
    }

}
